#ifndef EVENT_H
#define EVENT_H

#include <unordered_set>

namespace event {
    template<typename E>
    class ListenerSet;
    template<typename E>
    class Listener;

    template<typename E>
    class ListenerSet {
        friend class Listener<E>;

        static std::unordered_set<Listener<E>*> listeners;
    public:
        static void dispatch(E& event);
    };

    template<typename E>
    std::unordered_set<Listener<E>*> ListenerSet<E>::listeners;

    template<typename E>
    class Listener {
        friend class ListenerSet<E>;
    protected:
        virtual void receive(E& event) = 0;
    public:
        Listener();
        Listener(const Listener& other);
        ~Listener();
    };

    // ListenerSet

    template<typename E>
    void ListenerSet<E>::dispatch(E& event) {
        for (Listener<E>* l : listeners) l->receive(event);
    }

    // Listener

    template<typename E>
    Listener<E>::Listener() {
        ListenerSet<E>::listeners.insert(this);
    }

    template<typename E>
    Listener<E>::Listener(const Listener<E>& other) {
        ListenerSet<E>::listeners.insert(this);
    }

    template<typename E>
    Listener<E>::~Listener() {
        ListenerSet<E>::listeners.erase(this);
    }

    template<typename E>
    E dispatch(E e) {
        ListenerSet<E>::dispatch(e);
        return e;
    }

}

#endif