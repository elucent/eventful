# Eventful

Eventful is a simple C++ library for event handling. It allows you to dispatch
events of arbitrary types to an arbitrary number of runtime listeners.

Example use:
```cpp
#include  <iostream>

// Including the library.
#include "Event.h"

// Declaring an event type.
class SumEvent {
    int sum = 0;
public:
    void add(int n) {
        sum += n;
    }
    
    int getSum() const {
        return sum;
    }
};

// Declaring a listener.
class SumListener : public event::Listener<SumEvent> {
    // This must be protected! It isn't meant to be called by anything other
    // than an event dispatch.
protected:
    virtual void receive(SumEvent& event) {
        event.add(1);
    }
};

int main() {
    // Declare 5 listeners.
    SumListener a, b, c, d, e;
    
    // Send out our event.
    SumEvent sum = event::dispatch(SumEvent());
    std::cout << sum.getSum() << endl; // Should print 5.
}